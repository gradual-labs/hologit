---
layout: default
name: about
---
# about {{site.name}}

* [README]({{site.repourl}}/README.md)
* [index]({{site.gitpage}}/{{site.name}})
* [git]({{site.giturl}})
* [repository]({{site.repourl}})
* [pad](https://annuel.framapad.org/p/{{site.padname}})
* {{site.data.expires.expdate}}
* [hosting](https://host.io/{{site.domain}})
* [ipfs](https://gateway.ipfs.io{{site.data.dnslink}})
* contact: {{site.data.RP | replace: '.', '@'}}

* [related](https://host.io/ip/{{site.data.dns.a}})
* other holo*sites : on [netlify](https://app.netlify.com/teams/mychelium/sites?search=holo)

<!--
 https://host.io/api/domains/full/example.example?token=$TOKEN
 https://host.io/api/domains/googleanalytics/UA-61330992?limit=5&token=$TOKEN
 -->
