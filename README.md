---
layout: default
---
# holoGIT

* website: <https://hologit.ml>
* gitpage: <https://kinlabs.gitlab.io/hologit/>
* [distributedGIT](distributedGIT.html)


## Quick Hacky Way

readonly access to GIT repository via IPFS
and readwrite access via local [GITea] server

1. unpack git objects ...
2. do an update-server-info
3. add to ipfs

[more info](https://github.com/whyrusleeping/git-ipfs-rehost/blob/master/git-ipfs-rehost)

```sh
gitdir=$(git rev-parse --git-dir)
git gc
find $gitdir -name  *.pack > packs.lof
for f in $(cat packs.lof); do
cat $f | git unpack-objects
done
git update-server-info
ipfs add -r $gitdir

```

see also [*](https://theartofmachinery.com/2016/07/02/git_over_http.html)

## gitclient

* [GitJournal](https://gitjournal.io/) (iphone,android)
* [GitFork](https://git-fork.com/) (desktop: mac and windows)
* GitUp (mac)
* SourceTree,TortoiseGIT (windows)

* GitTouch (ipad,android)
* GitKraken (Board),
* Agit(earlyaccess), mGit, pockeyGit$2.3  (android)

* (GitDrive,gitHub app)

## git server 

 * Gitea
 * self hosted GitLab


-- <br>
[about](about.html)











