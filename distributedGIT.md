## Git, even more distributed

Have you ever said to yourself: "Man, my git server isn't distributed enough" or
"I wish I had an easy way to serve a static git repository worldwide". Well wish
no more, I have the solution for you!

In this article, I will be discussing how to serve a git repository through the
ipfs network. The end result will be a `git clone`able url served through IPFS!

To start, select a git repo you want to host, and do a bare clone of it:
```
$ git clone --bare git@gitlab.com/username/myrepo
```

For those who aren't super git savvy, a bare repo means that it doesn't have
a working tree, and can be used as a server. They have a slightly different
format than your normal git repo.

Now, to get it ready to be cloned, you need to do the following:
```
$ cd myrepo
$ git update-server-info
```

Optionally, you can unpack all of gits objects:
```
$ cp objects/pack/*.pack .
$ git unpack-objects < ./*.pack
$ rm ./*.pack
```

