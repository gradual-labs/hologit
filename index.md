---
layout: default
---

## ![holoGIT][logo] holoGIT : the _locallyCentralized_ GIT


## product: holoGit

objective: making a true decentralized<sup>*</sup> GIT,
where a collaborative repository can be shared serverless,
hence the name of locally centralized GIT !


Today GIT client instances often rely on a centralized repository to fetch from
and push to.
Even though it is possible to operate without a server, it requires
the knowledge of peer repositories and a mean of communication to synchronize
them such at sending patches over email or having pull request queues on a centralize server.

True decentralize "push" is difficult to achieve. Indeed is hard to have an "annonymously"
writable place where request can be collected without being spam.

We have develop a peer-to-peer Pull-Request Protocol
that doesn't requires ant central permissionned server.

is uses the [mychelium][1]'s [blockRing][2] over the [TNETwork][3]


By construction GIT objects are stored in a "content-addressable" manner and therefore
can easily be shared over and P2P network via IPFS for instance

see also : [hosting your GIT repository on IPFS](wiki:git-over-ipfs)

-- <br>
[about](about.html)

[1]: https://qwant.com/?q=mychelium+blockring+%26g
[2]: https://qwant.com/?q=%22blockRing%22+Technology+%26g
[3]: https://qwant.com/?q=TNET+Transparent+Network+%26g

[logo]: favicon.ico
